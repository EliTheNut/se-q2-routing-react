import logo from './logo.svg';
import './App.css';
import Home from './Components/Home/Home';
import Welcome from './Components/Welcome/Welcome';
import Navigation from './Components/Navigation/Navigation';
import Error from './Components/Error/Error';
import { BrowserRouter as Router, Route, Switch} from 'react-router-dom';

function App() {
  return (
    <Router>
      
      <div className="App">
        <Navigation/>
        <Switch>
        <Route 
        exact
        path="/" 
        component={Home}
        />
        <Route 
        path="/Welcome/:name" 
        component={Welcome}
        />
        <Route
        exact
        path="/Welcome"
        render={(props)=><Welcome {...props} name="Elijah"/>}
        />
        <Route
        component={Error}
        />
        </Switch>
      </div>
      
    </Router>
  );
}

export default App;
