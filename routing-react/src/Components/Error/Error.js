import React, {Component, Fragment} from 'react';

class Error extends Component{
    render(){
        return(
            <Fragment>
                <h1>404 ERROR</h1>
                <p>Page Not Found</p>
            </Fragment>
        );
    }
}
export default Error