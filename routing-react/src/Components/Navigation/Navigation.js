
import { Link } from 'react-router-dom';
import React, { Component } from 'react'

class Navigation extends Component{
    render(){
        return(
            <div class="Nav">
                <ul>
                    <li><Link to="/">Home</Link></li>
                    <li><Link to="/Welcome">Welcome</Link></li>
                </ul>
            </div>
        );
    }
}
export default Navigation;